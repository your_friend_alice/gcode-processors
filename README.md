# G-code Processors

## Disclaimer

G-code is the low level code that tells your 3D printer (or other machine) how to move. It is very easy to write G-code that can harm your machine, by overheating it, crashing its head into the build plate or a printed object, wasting material, and all sorts of other ills. None of scripts in this repo are designed to destroy your printer, but it is possible that bugs could exist in these scripts that could cause unintended behavior. I cannot take responsibility for your safety, or the safety of your machine. Consider inspecting the output (you can run the scripts manually on a copy of your G-code and look at what changed with a diff tool). If you know Python, take a look at the source code and make sure I haven't made some terrible mistake. Don't leave your machine running unattended, or at least, don't leave it running unattended while doing something new, or at very least, don't leave it running unattended and then blame me if something goes wrong and it burns to the ground. Just, see the warranty section of the license.

## mid-colorchange.py

Allows you to insert a color change (`M601`) midway through a layer, specifying a set of feature types to print either before or after the color change. You can insert a `MID_COLORCHANGE` command in PrusaSlicer's color change slider; move the cursor to the layer you want a color change to occur within, right click, and select "Custom G-code".

This allows you to sort of fake some of the effects multi-extruder printers are capable of with a single-extruder printer (within some tight restrictions).

### Commands

- `;MID_COLORCHANGE BEFORE=<type>,<...type>` to insert a color change, specifying which feature types to print *before* the color change (with all other feature types printing after).
- `;MID_COLORCHANGE AFTER=<type>,<...type>` to insert a color change, specifying which feature types to print *after* the color change (with all other feature types printing before).

`<type>` case-insensitively matches the "Feature Type"s shown in the PrusaSlicer UI. Examples include `Perimiter`, `External Perimeter`, `Solid Infill`, `Top solid infill`, `Skirt/Brim`. Types are comma separated and not quoted; leading and trailing whitespace is removed so format the list as you like (within a single line).

### Example

In layer 6, the following custom G-code is added:

```
;MID_COLORCHANGE BEFORE=external perimeter,perimeter
```

On layer 7, the following custom G-code is added:

```
;MID_COLORCHANGE AFTER=external perimeter,perimeter
```

After processing, the gcode will print as normal, until layer 6. At layer 6, the Extermal Perimeter and Perimeter features will be printed in the relative order specified by the slicer, then the printer will pause for a color change, and then the other feature types of layer 6 will print, also in the same relative order as that specified by the sliver. At layer 7, all the features *except for* the External Perimeter and Perimeter features will print, then the printer will pause for a color change, and then the External Perimeter and Perimeter features will be printed.

In this example, the model has an engraved pattern that cuts down to layer 8. The printer is run with white filament initially, black filament after the first color change, and back to white filament after the second color change. The result will be a model with uniform white perimeters, but 2 black layers visible through the pattern engraving.

### Warning

This processor can rearrange the order of "feature type" blocks in your G-code. This means that if you have *Print Settings > Output optons > Label objects* enabled, and any of the object start or end labels come after a `;TYPE:` line within a given layer will be moved with the rest of the code, potentially messing up the label. If you rely on those labels (for example, another processing script, or the CancelObject Octoprint plugin), this processor can break this functionality in unpredictable ways. Note that the processor does not touch any code outside layers that contain a `;MID_COLORCHANGE` command, so you can leave this processor enabled if you want and continue using object-label-based tools, just making sure to avoid those tools in cases where you have any `;MID_COLORCHANGE`s enabled.
