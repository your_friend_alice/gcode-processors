#!/usr/bin/env python3
import re
from collections import namedtuple
from lib import *

command = re.compile('^;MID_COLORCHANGE ([A-Z]+)=(.+)$')
space = re.compile(r'\s+')

positionValues = namedtuple('p', ('before', 'after'))('BEFORE', 'AFTER')

def endPosCode(data):
    pos = {'x': None, 'y': None, 'z': None, 'e': None}
    for line in data.splitlines():
        parts = re.split(space, line)
        if parts[0] in ('G0', 'G1'):
            for part in parts:
                if part[0].lower() in pos:
                    pos[part[0].lower()] = part[1:]
    out = 'G0'
    if pos['x'] is not None:
        out += ' X'+pos['x']
    if pos['y'] is not None:
        out += ' Y'+pos['y']
    return out

class Type:
    data = ""
    def __init__(self, name):
        self.name = name

    def setData(self, data):
        self.data = data

    def endPosCode(self):
        return endPosCode(self.data)

    def typeComment(self):
        return f';TYPE:{self.name}'

    def getData(self):
        return self.data.strip('\r\n')

    def __str__(self):
        return self.typeComment() + '\n' + self.getData()

if __name__ == '__main__':
    with streams() as (i, o):
        while True:
            _, match = skipToMatch(i, o, command, passthroughMatch=True)
            # break on EOF
            if match is None:
                break
            # parse command args
            positionArg = match.group(1)
            if positionArg not in positionValues:
                raise ValueError('MID_COLORCHANGE argument must start with either BEFORE= or AFTER=')
            typesArg = set(t.strip().lower() for t in match.group(2).split(',') if len(t.strip()) > 0)
            # seek to, parse first TYPE, create first Type object in the list
            layerStartCode = io.StringIO()
            pattern, match = skipToMatch(i, layerStartCode, patterns.typeStart, passthroughMatch=False)
            print(layerStartCode.getvalue(), file=o)
            if match is None:
                raise ValueError('Layer with MID_COLORCHANGE does not contain any gcode marked with a type!')
            types = [Type(match.group(1))]
            while True:
                # seek to the next TYPE comment line (or the end of the layer), capturing everything we seek over into the data field of the last Type in the list
                buf = io.StringIO()
                pattern, match = skipToMatch(i, buf, patterns.layerStart, patterns.typeStart, passthroughMatch=False)
                types[-1].setData(buf.getvalue())
                if pattern == patterns.typeStart:
                    # if we reached another TYPE line, parse it and create a new Type object from it
                    types.append(Type(match.group(1)))
                else:
                    # if we've reached the end of our current layer, it's time to process and print all the Type objects we've created
                    # Print captured gcode types for before color change
                    for index, t in enumerate(types):
                        if (positionArg == positionValues.before and t.name.lower() in typesArg) or (positionArg == positionValues.after and t.name.lower() not in typesArg):
                            print(f'Before: {t.name}')
                            print(t.typeComment(), file=o)
                            if index == 0:
                                print(endPosCode(layerStartCode.getvalue()), file=o)
                                print(endPosCode(layerStartCode.getvalue()))
                            else:
                                print(types[index-1].endPosCode(), file=o)
                                print(types[index-1].endPosCode())
                            print(t.getData(), file=o)
                    # Print color change
                    print(f';Added by {sys.argv[0]}:\nM600', file=o)
                    # Print captured gcode types for after color change
                    for index, t in enumerate(types):
                        if (positionArg == positionValues.before and t.name.lower() not in typesArg) or (positionArg == positionValues.after and t.name.lower() in typesArg):
                            print(f'After: {t.name}')
                            print(t.typeComment(), file=o)
                            if index > 0:
                                print(types[index-1].endPosCode(), file=o)
                                print(types[index-1].endPosCode())
                            print(t.getData(), file=o)
                    # Pass through the Layer Start line unchanged
                    print(match.group(0), file=o)
                    break
