import contextlib, io, os, re, sys

class patterns:
    layerStart = re.compile('^;LAYER_CHANGE$')
    typeStart = re.compile('^;TYPE:(.+)$')

@contextlib.contextmanager
def _stdioStreams():
    yield (sys.stdin, sys.stdout)

@contextlib.contextmanager
def _fileStreams(path):
    pathParts = (os.path.dirname(path), os.path.basename(path))
    infile = os.path.join(*pathParts)
    outfile = os.path.join(pathParts[0], f".{pathParts[1]}.tmp")
    with open(infile, 'r') as i:
        with open(outfile, 'w') as o:
            yield (i, o)
    os.rename(outfile, infile)

@contextlib.contextmanager
def streams():
    if len(sys.argv) == 2:
        with _fileStreams(sys.argv[1]) as tup:
            yield tup
    elif len(sys.argv) == 1 and sys.argv not in ("--help", "-h"):
        with _stdioStreams() as tup:
            yield tup
    else:
        print(f"Usage: {sys.argv[0]} [FILE]", file=sys.stdout)
        sys.exit(1)

def cleanLines(f):
    for line in f:
        yield line.rstrip('\r\n')

def skipToMatch(i, o, *pattern, passthroughMatch=True):
    for line in cleanLines(i):
        for p in pattern:
            match = re.match(p, line)
            if match:
                if passthroughMatch:
                    print(line, file=o)
                return (p, match)
        print(line, file=o)
    return (None, None)
