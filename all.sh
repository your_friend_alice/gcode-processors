#!/bin/bash
set -ex
cd "$(dirname "$0")"
for processor in processors/*.py; do
    "$processor" "$1"
done
